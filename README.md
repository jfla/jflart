Le paquet `jflart` fournit la classe LaTeX utilisée pour les articles
soumis et acceptés aux _Journées Francophones des Langages
Applicatifs_ (JFLA).

Le bref manuel disponible joue également le rôle d'exemple
d'utilisation de la classe, car il utilise lui-même la classe
`jflart`.

Le fichier source correspondant est [jflart.tex](jflart.tex). Un
fichier `Makefile` est aussi fourni pour compiler ce fichier source
vers un format `pdf`.

Il est recommandé d'utiliser une version récente de LaTeX pour le bon
fonctionnement de la classe.

