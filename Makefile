PACKAGE=jflart
DOCUMENTS=$(PACKAGE).pdf
FILES=$(PACKAGE).cls $(PACKAGE).tex $(PACKAGE).bib jfla.jpg $(DOCUMENTS)
ARCHIVE=$(PACKAGE).zip
TARGETS=$(DOCUMENTS) $(ARCHIVE)

.PHONY: all clean cleanall

all: $(TARGETS)

$(ARCHIVE): $(FILES)
	zip -o $@ $^

clean:
	rm -f *.{log,aux,glo,idx,toc,out,lof,lot,bbl,blg,gls,cut,hd,thm}
	rm -f *.{run.xml,fls,fdb_latexmk,bcf}

cleanall: clean
	rm -f $(TARGETS)

%.pdf: %.tex %.bib *.cls
	pdflatex $<
	- bibtex $*
	pdflatex $<
	pdflatex $<
